<?php
get_header(); ?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
		if ( have_posts() ) { ?>

			<header class="page-header">
				<h1 class="page-title"><?php printf( esc_html__( 'Search Results for: %s', 'bluebox' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
			</header><!-- .page-header -->

			<?php

			while ( have_posts() ) {
				the_post();
				get_template_part( 'template-parts/content', 'search' );

			}

			the_posts_navigation();

		} else {

			get_template_part( 'template-parts/content', 'none' );

		} ?>

		</main>
	</section>

<?php
get_sidebar();
get_footer();
