<?php

if (!function_exists('setup')) {

	function initThemeSetup() {
		load_theme_textdomain( 'bluebox', get_template_directory() . '/languages' );

		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'title-tag' );
		add_theme_support( 'post-thumbnails' );


		register_nav_menus( [
			'primary' => esc_html__( 'Primary', 'bluebox' ),
		] );

		add_theme_support( 'html5', [
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		] );

		add_theme_support( 'custom-background', apply_filters( 'bluebox_custom_background_args', [
			'default-color' => 'ffffff',
			'default-image' => '',
		] ) );
	}

	add_action( 'after_setup_theme', 'initThemeSetup' );

	function initWidgets() {
		register_sidebar( [
			'name'          => esc_html__( 'Sidebar', 'bluebox' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'bluebox' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		] );
	}

	add_action( 'widgets_init', 'initWidgets' );


	function initScripts() {
		wp_enqueue_style( 'bluebox-style', get_stylesheet_uri() );
		wp_enqueue_script( 'bluebox-navigation', get_template_directory_uri() . '/js/navigation.js', [], '20151215', true );
		wp_enqueue_script( 'bluebox-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', [], '20151215', true );
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
	}

	add_action( 'wp_enqueue_scripts', 'initScripts' );}
