</div>

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info">
			<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'bluebox' ) ); ?>"><?php printf( esc_html__( 'Proudly powered by %s', 'bluebox' ), 'WordPress' ); ?></a>
			<span class="sep"> | </span>
			<?php printf( esc_html__( 'Theme: %1$s by %2$s.', 'bluebox' ), 'bluebox', '<a href="http://www.wilhelmsempre.pl" rel="designer">Rafał Głuszak</a>' ); ?>
		</div>
	</footer>
</div>

<?php wp_footer(); ?>

</body>
</html>
